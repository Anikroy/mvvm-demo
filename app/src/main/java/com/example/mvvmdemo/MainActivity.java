package com.example.mvvmdemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.mvvmdemo.databinding.ActivityMainBinding;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    PostViewModel viewModel;
    ActivityMainBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_main);
         viewModel = new ViewModelProvider(this).get(PostViewModel.class);
         getProgressBar();
        viewModel.getPosts().observe(MainActivity.this, posts -> {

            if (posts != null){
                if (posts.size()>0){
                    binding.recyclerViewID.setVisibility(View.VISIBLE);
                    PostAdapter adapter = new PostAdapter(MainActivity.this,posts);
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
                    binding.recyclerViewID.setLayoutManager(layoutManager);
                    binding.recyclerViewID.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
            }

            else {
                Toast.makeText(MainActivity.this, "No data", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getProgressBar(){
        viewModel.getProgressBar().observe(MainActivity.this, aBoolean -> {

            if (aBoolean){
                binding.progressBarID.setVisibility(View.VISIBLE);
                Log.d("TAG", "getProgressBar: Visible");

            }else {
                binding.progressBarID.setVisibility(View.GONE);
                Log.d("TAG", "getProgressBar: Gone");

            }

        });
    }
}