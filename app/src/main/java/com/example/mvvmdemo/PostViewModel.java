package com.example.mvvmdemo;

import android.app.Application;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

public class PostViewModel extends AndroidViewModel {

    private PostRepository postRepository;
    public PostViewModel(@NonNull Application application) {
        super(application);
        postRepository = new PostRepository(application);
    }

    public MutableLiveData<List<Post>>getPosts(){


        return postRepository.getPosts();
    }

    public MutableLiveData<Boolean> getProgressBar(){

        return postRepository.getProgressbarObservable();
    }
}
