package com.example.mvvmdemo;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostRepository {

    private MutableLiveData<List<Post>> mutableLiveData;
    private Application application;
    private ApiInterface apiInterface;
    public MutableLiveData<Boolean> progressbarObservable;


    public PostRepository(Application application) {
        this.application = application;
        apiInterface = ApiClient.getRetrofitInstance().create(ApiInterface.class);
        progressbarObservable = new MutableLiveData<>();


    }

    public MutableLiveData<List<Post>> getPosts() {

        try {
            progressbarObservable.postValue(true);
            mutableLiveData = new MutableLiveData<>();

            Call<List<Post>> postResponse = apiInterface.getPosts();
            postResponse.enqueue(new Callback<List<Post>>() {
                @Override
                public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                    if (response.body() != null) {
                        if (response.code() == 200) {
                            progressbarObservable.postValue(false);
                            mutableLiveData.postValue(response.body());
                            Log.d("Call", "onResponse:calling ");

                        } else {
                            mutableLiveData.postValue(response.body());
                        }
                    }
                }

                @Override
                public void onFailure(Call<List<Post>> call, Throwable t) {
                    progressbarObservable.postValue(false);

                }
            });

        } catch (Exception e) {
            Log.d("Error Line Number", Log.getStackTraceString(e));
        }


        return mutableLiveData;

    }

    public MutableLiveData<Boolean> getProgressbarObservable() {

        return progressbarObservable;
    }

}
